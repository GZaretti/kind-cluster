# Using LoadBalancer

Apply the content !
```
kubectl apply -f loadbalancer-usage.yaml
```

Verify that the loadbalancer worksby sending traffic to it's external IP and port.
```
LB_IP=$(kubectl get svc/foo-service -o=jsonpath='{.status.loadBalancer.ingress[0].ip}')
```
```
# should output foo and bar on separate lines 
for _ in {1..10}; do
  curl ${LB_IP}:5678
done
```
