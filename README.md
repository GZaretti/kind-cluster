# Createing a kind cluster

```
kind create cluster --config kind-config.yaml
```

## Install ingress NGINX

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
``` 

Now the ingress is all setup, setup with kind-config.yaml and apply ingress-nginx. Wait until is ready to process requests running.
```
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s
```

```
kubectl cluster-info --context kind-kind
```

## Install loadbalancer with metallb

Creating metallb namespace
```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/namespace.yaml
```
Creating the memberlist secrets manifest
```
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
```
apply metallb manifest
```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml
```
wait for metallb pods to have a status of Running
```
kubectl get pods -n metallb-system --watch
```

### SetUp metallb address pool used by loadbalancers
```
docker network inspect -f '{{.IPAM.Config}}' kind
```

Copy the cidr on this config and apply it (kubectl apply -f metallb-configmap.yaml):

apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 172.19.255.200-172.19.255.250

### Using load balancer
see [`UsingLoadBalancer`](UsingLoadBalancer)

